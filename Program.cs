using System;
using System.Collections.Generic;
using System.Threading;
using System.Runtime;

namespace Sea_Battle
{
	class PartOfGameMap
	{
		public bool PartIsFull { get; set; }

		public bool PartHullOfShipIsNotDestroed { get; set; }

		public string PartWhoContainWhatUserAreWatcing { get; set; }
	}
	class Program
	{
		public static void CreateMap(int height, int width, PartOfGameMap[,] partOfGameMapPlayer, PartOfGameMap[,] partOfGameMapComputer)
		{
			string dictrionary = " ABCDEFGHIJ";

			partOfGameMapPlayer[0, 0] = new PartOfGameMap();

			partOfGameMapPlayer[0, 0].PartWhoContainWhatUserAreWatcing = " ";

			for (int i = 1; i < height; i++)
			{
				partOfGameMapPlayer[i,0] = new PartOfGameMap();

				partOfGameMapPlayer[0, i] = new PartOfGameMap();

				partOfGameMapPlayer[i, 0].PartWhoContainWhatUserAreWatcing =(i - 1).ToString();

				partOfGameMapPlayer[0, i].PartWhoContainWhatUserAreWatcing = dictrionary[i].ToString();
			}

			for (int i = 1; i < height; i++)
			{
				for(int j = 1; j < width; j++)
				{
					partOfGameMapPlayer[i, j] = new PartOfGameMap();

					partOfGameMapPlayer[i, j].PartIsFull = false;

					partOfGameMapPlayer[i, j].PartHullOfShipIsNotDestroed = false;

					partOfGameMapPlayer[i, j].PartWhoContainWhatUserAreWatcing = "0";					
				}
			}

			partOfGameMapComputer[0, 0] = new PartOfGameMap();

			partOfGameMapComputer[0, 0].PartWhoContainWhatUserAreWatcing = " ";

			for (int i = 1; i < height; i++)
			{
				partOfGameMapComputer[i, 0] = new PartOfGameMap();

				partOfGameMapComputer[0, i] = new PartOfGameMap();

				partOfGameMapComputer[i, 0].PartWhoContainWhatUserAreWatcing = (i - 1).ToString();

				partOfGameMapComputer[0, i].PartWhoContainWhatUserAreWatcing = dictrionary[i].ToString();
			}

			for (int i = 1; i < height; i++)
			{
				for (int j = 1; j < width; j++)
				{
					partOfGameMapComputer[i, j] = new PartOfGameMap();

					partOfGameMapComputer[i, j].PartIsFull = false;

					partOfGameMapComputer[i, j].PartHullOfShipIsNotDestroed = false;

					partOfGameMapComputer[i, j].PartWhoContainWhatUserAreWatcing = "B";
				}
			}
		}

		public static void RenderBattleMaps(int height, int width, PartOfGameMap[,] partOfGameMapPlayer, PartOfGameMap[,] partOfGameMapComputer)
		{
			for (int i = 0; i < height; i++)
			{
				for(int j = 0; j < width; j++)
				{
					Console.Write(partOfGameMapPlayer[i, j].PartWhoContainWhatUserAreWatcing + " ");
				}
				Console.WriteLine();
			}

			Console.WriteLine();

			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					Console.Write(partOfGameMapComputer[i, j].PartWhoContainWhatUserAreWatcing + " ");
				}
				Console.WriteLine();
			}
		}

		public static void RenderPlayerBattleMap(int height, int width, PartOfGameMap[,] partOfGameMapPlayer)
		{
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					Console.Write(partOfGameMapPlayer[i, j].PartWhoContainWhatUserAreWatcing + " ");
				}
				Console.WriteLine();
			}

			Console.WriteLine();
		}

		public static void SetUpComputerShips(PartOfGameMap[,] partOfGameMapPlayer, PartOfGameMap[,] partOfGameMapComputer)
		{
			

			for(int i = 1; i < 11; i++)
			{
				for(int j = 1; j < 11; j++)
				{
					partOfGameMapComputer[i, j].PartHullOfShipIsNotDestroed = partOfGameMapPlayer[i, j].PartHullOfShipIsNotDestroed;

					partOfGameMapComputer[i, j].PartIsFull = partOfGameMapPlayer[i, j].PartIsFull;

					partOfGameMapComputer[i, j].PartWhoContainWhatUserAreWatcing = partOfGameMapPlayer[i, j].PartWhoContainWhatUserAreWatcing;
				}
			}
		}

		static void Main(string[] args)
		{
			static int GenerateDigit(Random rnd)
			{
				Thread.Sleep(19);

				return rnd.Next(1, 10);
			}

			#region Dictionaries for number and letter coordinates
			Dictionary<int, int> dictionaryForNumbers = new Dictionary<int, int>();

			dictionaryForNumbers.Add(0, 1);
			dictionaryForNumbers.Add(1, 2);
			dictionaryForNumbers.Add(2, 3);
			dictionaryForNumbers.Add(3, 4);
			dictionaryForNumbers.Add(4, 5);
			dictionaryForNumbers.Add(5, 6);
			dictionaryForNumbers.Add(6, 7);
			dictionaryForNumbers.Add(7, 8);
			dictionaryForNumbers.Add(8, 9);
			dictionaryForNumbers.Add(9, 10);

			Dictionary<string, int> dictionaryForLetters = new Dictionary<string, int>();

			dictionaryForLetters.Add("A", 1);
			dictionaryForLetters.Add("B", 2);
			dictionaryForLetters.Add("C", 3);
			dictionaryForLetters.Add("D", 4);
			dictionaryForLetters.Add("E", 5);
			dictionaryForLetters.Add("F", 6);
			dictionaryForLetters.Add("G", 7);
			dictionaryForLetters.Add("H", 8);
			dictionaryForLetters.Add("I", 9);
			dictionaryForLetters.Add("J", 10);
			#endregion

			PartOfGameMap[,] partOfGameMapPlayer = new PartOfGameMap[11, 11];

			PartOfGameMap[,] partOfGameMapComputer = new PartOfGameMap[11,11];

			CreateMap(11,11, partOfGameMapPlayer, partOfGameMapComputer);

			RenderPlayerBattleMap(11, 11, partOfGameMapPlayer);

			int setUpShipCoordinateX = 0;

			string stringCoordinateY = "";

			int setUpShipCoordinateY = 0;

			bool tryingToReadYCoordinate = false;

			bool tryingToPushArrowKey = false;

			bool leftRightFrontPartIsEmpty = false;

			Console.WriteLine("Расставьте корабли вводя координаты с клавиатуры");

			for (int countOfSetUpedShips = 0; countOfSetUpedShips < 10; countOfSetUpedShips++)
			{
				if (countOfSetUpedShips == 0)
				{
					bool tryingToSetUpShip = false;

					while (!tryingToSetUpShip)
					{
						setUpShipCoordinateX = 0;

						stringCoordinateY = "";

						setUpShipCoordinateY = 0;

						bool tryingToSetUpXCoordinate = false;

						while (!tryingToSetUpXCoordinate)
						{
							Console.WriteLine("Введите циферную координату");

							while (!int.TryParse(Console.ReadLine(), out setUpShipCoordinateX))
							{
								Console.WriteLine("Вводите только цифры");

								Console.WriteLine("Введите циферную координату");
							}

							foreach(KeyValuePair<int,int> keyValue in dictionaryForNumbers)
							{
								if(keyValue.Key == setUpShipCoordinateX)
								{
									setUpShipCoordinateX = keyValue.Value;

									break;
								}
							}

							if (setUpShipCoordinateX > 1 && setUpShipCoordinateX < 10)
							{
								tryingToSetUpXCoordinate = true;
							}
							else
							{
								Console.WriteLine("Еще раз");
							}
						}

						bool tryingToSetUpYCoordinate = false;

						while (!tryingToSetUpYCoordinate)
						{
							Console.WriteLine("Введите буквенную координату");

							tryingToReadYCoordinate = false;

							while (!tryingToReadYCoordinate)
							{
								stringCoordinateY = Console.ReadLine();

								foreach(KeyValuePair<string, int> keyValue in dictionaryForLetters)
								{
									if(keyValue.Key == stringCoordinateY)
									{
										setUpShipCoordinateY = keyValue.Value;

										tryingToReadYCoordinate = true;

										break;
									}
								}

								if(!tryingToReadYCoordinate)
								{
									Console.WriteLine("Вводите только указанные на поле буквы");

									Console.WriteLine("Введите буквенную координату");
								}								
							}

							if (setUpShipCoordinateY > 1 && setUpShipCoordinateY < 10)
							{
								if(partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX + 1, setUpShipCoordinateY].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX - 1, setUpShipCoordinateY].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY + 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY - 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX - 1, setUpShipCoordinateY - 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX - 1, setUpShipCoordinateY + 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX + 1, setUpShipCoordinateY - 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX - 1, setUpShipCoordinateY + 1].PartIsFull == false)
								{
									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "A";

									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = true;

									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = true;

									tryingToSetUpYCoordinate = true;
								}
								else
								{
									Console.WriteLine("Либо не правильное значение, либо попытка установить корабль поверх уже существующего");
								}
							}
							else
							{
								Console.WriteLine("Либо не правильное значение, либо попытка установить корабль поверх уже существующего");
							}
						}

						Console.Clear();
						
						RenderPlayerBattleMap(11, 11, partOfGameMapPlayer);

						Console.WriteLine("Вы установили опорную точку для авианосца(Aircarrier - 4 палубы)");

						Console.WriteLine("Теперь, при помощи стрелок на клавиатуре, выберите направление, куда будет развернут корабль");

						tryingToPushArrowKey = false;

						leftRightFrontPartIsEmpty = false;

						while(!tryingToPushArrowKey)
						{
							switch (Console.ReadKey(true).Key)
							{
								case ConsoleKey.UpArrow:
									{
										tryingToPushArrowKey = true;

										if (setUpShipCoordinateX - 4 > 0)
										{
											for(int i = setUpShipCoordinateX; i > setUpShipCoordinateX - 4; i-- )
											{
												leftRightFrontPartIsEmpty = true;

												if(partOfGameMapPlayer[i - 1, setUpShipCoordinateY].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY + 1].PartIsFull == true ||
												   partOfGameMapPlayer[i - 1, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i - 1, setUpShipCoordinateY + 1].PartIsFull == true)
												{
													leftRightFrontPartIsEmpty = false;

													break;
												}
											}

											if(leftRightFrontPartIsEmpty)
											{
												for (int x = setUpShipCoordinateX; x > setUpShipCoordinateX - 4; x--)
												{
													partOfGameMapPlayer[x, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "A";

													partOfGameMapPlayer[x, setUpShipCoordinateY].PartIsFull = true;

													partOfGameMapPlayer[x, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = true;
												}

												tryingToSetUpShip = true;
											}
											else
											{
												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
											}
										}
										else
										{
											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
										}

										Console.Clear();

										RenderPlayerBattleMap(11, 11, partOfGameMapPlayer);

										break;
									}

								case ConsoleKey.DownArrow:
									{
										tryingToPushArrowKey = true;

										if (setUpShipCoordinateX + 4 < 11)
										{
											for (int i = setUpShipCoordinateX; i < setUpShipCoordinateX + 4; i++)
											{
												leftRightFrontPartIsEmpty = true;

												if (partOfGameMapPlayer[i + 1, setUpShipCoordinateY].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY + 1].PartIsFull == true ||
												   partOfGameMapPlayer[i + 1, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i + 1, setUpShipCoordinateY + 1].PartIsFull == true)
												{
													leftRightFrontPartIsEmpty = false;

													break;
												}
											}


											if(leftRightFrontPartIsEmpty)
											{
												for (int x = setUpShipCoordinateX; x < setUpShipCoordinateX + 4; x++)
												{
													if (partOfGameMapPlayer[x, setUpShipCoordinateY].PartIsFull == false)
													{
														partOfGameMapPlayer[x, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "A";

														partOfGameMapPlayer[x, setUpShipCoordinateY].PartIsFull = true;

														partOfGameMapPlayer[x, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = true;
													}

												}

												tryingToSetUpShip = true;
											}
											else
											{
												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
											}

										}
										else
										{
											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
										}

										Console.Clear();

										RenderPlayerBattleMap(11, 11, partOfGameMapPlayer);

										break;
									}

								case ConsoleKey.LeftArrow:
									{
										tryingToPushArrowKey = true;

										if (setUpShipCoordinateY - 4 > 0)
										{
											for (int i = setUpShipCoordinateY; i > setUpShipCoordinateY - 4; i--)
											{
												leftRightFrontPartIsEmpty = true;

												if (partOfGameMapPlayer[i - 1, setUpShipCoordinateY].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY + 1].PartIsFull == true ||
												   partOfGameMapPlayer[i - 1, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i + 1, setUpShipCoordinateY - 1].PartIsFull == true)
												{
													leftRightFrontPartIsEmpty = false;

													break;
												}
											}

											if(leftRightFrontPartIsEmpty)
											{
												for (int y = setUpShipCoordinateY; y > setUpShipCoordinateY - 4; y--)
												{
													partOfGameMapPlayer[setUpShipCoordinateX, y].PartWhoContainWhatUserAreWatcing = "A";

													partOfGameMapPlayer[setUpShipCoordinateX, y].PartIsFull = true;

													partOfGameMapPlayer[setUpShipCoordinateX, y].PartHullOfShipIsNotDestroed = true;
												}

												tryingToSetUpShip = true;
											}
											else
											{
												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
											}
										}
										else
										{
											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
										}

										Console.Clear();

										RenderPlayerBattleMap(11, 11, partOfGameMapPlayer);

										break;
									}

								case ConsoleKey.RightArrow:
									{
										tryingToPushArrowKey = true;

										if (setUpShipCoordinateY + 4 < 11)
										{
											for (int i = setUpShipCoordinateY; i < setUpShipCoordinateY + 4; i++)
											{
												leftRightFrontPartIsEmpty = true;

												if (partOfGameMapPlayer[i + 1, setUpShipCoordinateY].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY + 1].PartIsFull == true ||
												   partOfGameMapPlayer[i - 1, setUpShipCoordinateY + 1].PartIsFull == true ||
												   partOfGameMapPlayer[i + 1, setUpShipCoordinateY + 1].PartIsFull == true)
												{
													leftRightFrontPartIsEmpty = false;

													break;
												}
											}

											if(leftRightFrontPartIsEmpty)
											{
												for (int y = setUpShipCoordinateY; y < setUpShipCoordinateY + 4; y++)
												{
													partOfGameMapPlayer[setUpShipCoordinateX, y].PartWhoContainWhatUserAreWatcing = "A";

													partOfGameMapPlayer[setUpShipCoordinateX, y].PartIsFull = true;

													partOfGameMapPlayer[setUpShipCoordinateX, y].PartHullOfShipIsNotDestroed = true;
												}

												tryingToSetUpShip = true;
											}
											else
											{
												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
											}
										}
										else
										{
											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
										}

										Console.Clear();

										RenderPlayerBattleMap(11, 11, partOfGameMapPlayer);

										break;
									}
							}
						}						
					}
				}

				if (countOfSetUpedShips == 1 || countOfSetUpedShips == 2)
				{
					bool tryingToSetUpShip = false;

					while (!tryingToSetUpShip)
					{
						setUpShipCoordinateX = 0;

						stringCoordinateY = "";

						setUpShipCoordinateY = 0;

						bool tryingToSetUpXCoordinate = false;

						while (!tryingToSetUpXCoordinate)
						{
							Console.WriteLine("Введите циферную координату");

							while (!int.TryParse(Console.ReadLine(), out setUpShipCoordinateX))
							{
								Console.WriteLine("Вводите только цифры");

								Console.WriteLine("Введите циферную координату");
							}

							foreach (KeyValuePair<int, int> keyValue in dictionaryForNumbers)
							{
								if (keyValue.Key == setUpShipCoordinateX)
								{
									setUpShipCoordinateX = keyValue.Value;

									break;
								}
							}

							if (setUpShipCoordinateX > 1 && setUpShipCoordinateX < 10)
							{
								tryingToSetUpXCoordinate = true;
							}
							else
							{
								Console.WriteLine("Еще раз");
							}
						}

						bool tryingToSetUpYCoordinate = false;

						while (!tryingToSetUpYCoordinate)
						{
							Console.WriteLine("Введите буквенную координату");

							tryingToReadYCoordinate = false;

							while (!tryingToReadYCoordinate)
							{
								stringCoordinateY = Console.ReadLine();

								foreach (KeyValuePair<string, int> keyValue in dictionaryForLetters)
								{
									if (keyValue.Key == stringCoordinateY)
									{
										setUpShipCoordinateY = keyValue.Value;

										tryingToReadYCoordinate = true;

										break;
									}
								}

								if (!tryingToReadYCoordinate)
								{
									Console.WriteLine("Вводите только указанные на поле буквы");

									Console.WriteLine("Введите буквенную координату");
								}
							}

							if (setUpShipCoordinateY > 1 && setUpShipCoordinateY < 10)
							{
								if (partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX + 1, setUpShipCoordinateY].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX - 1, setUpShipCoordinateY].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY + 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY - 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX - 1, setUpShipCoordinateY - 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX - 1, setUpShipCoordinateY + 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX + 1, setUpShipCoordinateY - 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX - 1, setUpShipCoordinateY + 1].PartIsFull == false)
								{
									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "C";

									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = true;

									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = true;

									tryingToSetUpYCoordinate = true;
								}
								else
								{
									Console.WriteLine("Либо не правильное значение, либо попытка установить корабль поверх уже существующего");
								}
							}
							else
							{
								Console.WriteLine("Либо не правильное значение, либо попытка установить корабль поверх уже существующего");
							}
						}

						Console.Clear();

						RenderPlayerBattleMap(11, 11, partOfGameMapPlayer);

						Console.WriteLine("Вы установили опорную точку для крейсера(Cruiser - 3 палубы)");

						Console.WriteLine("Теперь, при помощи стрелок на клавиатуре, выберите направление, куда будет развернут корабль");

						tryingToPushArrowKey = false;

						leftRightFrontPartIsEmpty = false;

						while (!tryingToPushArrowKey)
						{
							switch (Console.ReadKey(true).Key)
							{
								case ConsoleKey.UpArrow:
									{
										tryingToPushArrowKey = true;

										if (setUpShipCoordinateX - 3 > 0)
										{
											for (int i = setUpShipCoordinateX; i > setUpShipCoordinateX - 3; i--)
											{
												leftRightFrontPartIsEmpty = true;

												if (partOfGameMapPlayer[i - 1, setUpShipCoordinateY].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY + 1].PartIsFull == true ||
												   partOfGameMapPlayer[i - 1, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i - 1, setUpShipCoordinateY + 1].PartIsFull == true)
												{
													leftRightFrontPartIsEmpty = false;

													break;
												}
											}

											if (leftRightFrontPartIsEmpty)
											{
												for (int x = setUpShipCoordinateX; x > setUpShipCoordinateX - 3; x--)
												{
													partOfGameMapPlayer[x, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "C";

													partOfGameMapPlayer[x, setUpShipCoordinateY].PartIsFull = true;

													partOfGameMapPlayer[x, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = true;
												}

												tryingToSetUpShip = true;
											}
											else
											{
												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
											}
										}
										else
										{
											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
										}

										Console.Clear();

										RenderPlayerBattleMap(11, 11, partOfGameMapPlayer);

										break;
									}

								case ConsoleKey.DownArrow:
									{
										tryingToPushArrowKey = true;

										if (setUpShipCoordinateX + 3 < 11)
										{
											for (int i = setUpShipCoordinateX; i < setUpShipCoordinateX + 3; i++)
											{
												leftRightFrontPartIsEmpty = true;

												if (partOfGameMapPlayer[i + 1, setUpShipCoordinateY].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY + 1].PartIsFull == true ||
												   partOfGameMapPlayer[i + 1, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i + 1, setUpShipCoordinateY + 1].PartIsFull == true)
												{
													leftRightFrontPartIsEmpty = false;

													break;
												}
											}


											if (leftRightFrontPartIsEmpty)
											{
												for (int x = setUpShipCoordinateX; x < setUpShipCoordinateX + 3; x++)
												{
													if (partOfGameMapPlayer[x, setUpShipCoordinateY].PartIsFull == false)
													{
														partOfGameMapPlayer[x, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "C";

														partOfGameMapPlayer[x, setUpShipCoordinateY].PartIsFull = true;

														partOfGameMapPlayer[x, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = true;
													}

												}

												tryingToSetUpShip = true;
											}
											else
											{
												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
											}
										}
										else
										{
											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
										}

										Console.Clear();

										RenderPlayerBattleMap(11, 11, partOfGameMapPlayer);

										break;
									}

								case ConsoleKey.LeftArrow:
									{
										tryingToPushArrowKey = true;

										if (setUpShipCoordinateY - 3 > 0)
										{
											for (int i = setUpShipCoordinateY; i > setUpShipCoordinateY - 3; i--)
											{
												leftRightFrontPartIsEmpty = true;

												if (partOfGameMapPlayer[i - 1, setUpShipCoordinateY].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY + 1].PartIsFull == true ||
												   partOfGameMapPlayer[i - 1, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i + 1, setUpShipCoordinateY - 1].PartIsFull == true)
												{
													leftRightFrontPartIsEmpty = false;

													break;
												}
											}

											if (leftRightFrontPartIsEmpty)
											{
												for (int y = setUpShipCoordinateY; y > setUpShipCoordinateY - 3; y--)
												{
													partOfGameMapPlayer[setUpShipCoordinateX, y].PartWhoContainWhatUserAreWatcing = "C";

													partOfGameMapPlayer[setUpShipCoordinateX, y].PartIsFull = true;

													partOfGameMapPlayer[setUpShipCoordinateX, y].PartHullOfShipIsNotDestroed = true;
												}

												tryingToSetUpShip = true;
											}
											else
											{
												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
											}
										}
										else
										{
											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
										}

										Console.Clear();

										RenderPlayerBattleMap(11, 11, partOfGameMapPlayer);

										break;
									}

								case ConsoleKey.RightArrow:
									{
										tryingToPushArrowKey = true;

										if (setUpShipCoordinateY + 3 < 11)
										{
											for (int i = setUpShipCoordinateY; i < setUpShipCoordinateY + 3; i++)
											{
												leftRightFrontPartIsEmpty = true;

												if (partOfGameMapPlayer[i + 1, setUpShipCoordinateY].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY + 1].PartIsFull == true ||
												   partOfGameMapPlayer[i - 1, setUpShipCoordinateY + 1].PartIsFull == true ||
												   partOfGameMapPlayer[i + 1, setUpShipCoordinateY + 1].PartIsFull == true)
												{
													leftRightFrontPartIsEmpty = false;

													break;
												}
											}

											if (leftRightFrontPartIsEmpty)
											{
												for (int y = setUpShipCoordinateY; y < setUpShipCoordinateY + 3; y++)
												{
													partOfGameMapPlayer[setUpShipCoordinateX, y].PartWhoContainWhatUserAreWatcing = "C";

													partOfGameMapPlayer[setUpShipCoordinateX, y].PartIsFull = true;

													partOfGameMapPlayer[setUpShipCoordinateX, y].PartHullOfShipIsNotDestroed = true;
												}

												tryingToSetUpShip = true;
											}
											else
											{
												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
											}
										}
										else
										{
											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
										}

										Console.Clear();

										RenderPlayerBattleMap(11, 11, partOfGameMapPlayer);

										break;
									}
							}
						}
					}
				}

				if (countOfSetUpedShips == 3 || countOfSetUpedShips == 4 || countOfSetUpedShips == 5)
				{
					bool tryingToSetUpShip = false;

					while (!tryingToSetUpShip)
					{
						setUpShipCoordinateX = 0;

						stringCoordinateY = "";

						setUpShipCoordinateY = 0;

						bool tryingToSetUpXCoordinate = false;

						while (!tryingToSetUpXCoordinate)
						{
							Console.WriteLine("Введите циферную координату");

							while (!int.TryParse(Console.ReadLine(), out setUpShipCoordinateX))
							{
								Console.WriteLine("Вводите только цифры");

								Console.WriteLine("Введите циферную координату");
							}

							foreach (KeyValuePair<int, int> keyValue in dictionaryForNumbers)
							{
								if (keyValue.Key == setUpShipCoordinateX)
								{
									setUpShipCoordinateX = keyValue.Value;

									break;
								}
							}

							if (setUpShipCoordinateX > 1 && setUpShipCoordinateX < 10)
							{
								tryingToSetUpXCoordinate = true;
							}
							else
							{
								Console.WriteLine("Еще раз");
							}
						}

						bool tryingToSetUpYCoordinate = false;

						while (!tryingToSetUpYCoordinate)
						{
							Console.WriteLine("Введите буквенную координату");

							tryingToReadYCoordinate = false;

							while (!tryingToReadYCoordinate)
							{
								stringCoordinateY = Console.ReadLine();

								foreach (KeyValuePair<string, int> keyValue in dictionaryForLetters)
								{
									if (keyValue.Key == stringCoordinateY)
									{
										setUpShipCoordinateY = keyValue.Value;

										tryingToReadYCoordinate = true;

										break;
									}
								}

								if (!tryingToReadYCoordinate)
								{
									Console.WriteLine("Вводите только указанные на поле буквы");

									Console.WriteLine("Введите буквенную координату");
								}
							}

							if (setUpShipCoordinateY > 1 && setUpShipCoordinateY < 10)
							{
								if (partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX + 1, setUpShipCoordinateY].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX - 1, setUpShipCoordinateY].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY + 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY - 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX - 1, setUpShipCoordinateY - 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX - 1, setUpShipCoordinateY + 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX + 1, setUpShipCoordinateY - 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX - 1, setUpShipCoordinateY + 1].PartIsFull == false)
								{
									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "D";

									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = true;

									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = true;

									tryingToSetUpYCoordinate = true;
								}
								else
								{
									Console.WriteLine("Либо не правильное значение, либо попытка установить корабль поверх уже существующего");
								}
							}
							else
							{
								Console.WriteLine("Либо не правильное значение, либо попытка установить корабль поверх уже существующего");
							}
						}

						Console.Clear();

						RenderPlayerBattleMap(11, 11, partOfGameMapPlayer);

						Console.WriteLine("Вы установили опорную точку для эсминца(Destroer - 2 палубы)");

						Console.WriteLine("Теперь, при помощи стрелок на клавиатуре, выберите направление, куда будет развернут корабль");

						tryingToPushArrowKey = false;

						leftRightFrontPartIsEmpty = false;

						while (!tryingToPushArrowKey)
						{
							switch (Console.ReadKey(true).Key)
							{
								case ConsoleKey.UpArrow:
									{
										tryingToPushArrowKey = true;

										if (setUpShipCoordinateX - 2 > 0)
										{
											for (int i = setUpShipCoordinateX; i > setUpShipCoordinateX - 2; i--)
											{
												leftRightFrontPartIsEmpty = true;

												if (partOfGameMapPlayer[i - 1, setUpShipCoordinateY].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY + 1].PartIsFull == true ||
												   partOfGameMapPlayer[i - 1, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i - 1, setUpShipCoordinateY + 1].PartIsFull == true)
												{
													leftRightFrontPartIsEmpty = false;

													break;
												}
											}

											if (leftRightFrontPartIsEmpty)
											{
												for (int x = setUpShipCoordinateX; x > setUpShipCoordinateX - 2; x--)
												{
													partOfGameMapPlayer[x, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "D";

													partOfGameMapPlayer[x, setUpShipCoordinateY].PartIsFull = true;

													partOfGameMapPlayer[x, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = true;
												}

												tryingToSetUpShip = true;
											}
											else
											{
												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
											}
										}
										else
										{
											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
										}

										Console.Clear();

										RenderPlayerBattleMap(11, 11, partOfGameMapPlayer);

										break;
									}

								case ConsoleKey.DownArrow:
									{
										tryingToPushArrowKey = true;

										if (setUpShipCoordinateX + 2 < 11)
										{
											for (int i = setUpShipCoordinateX; i < setUpShipCoordinateX + 2; i++)
											{
												leftRightFrontPartIsEmpty = true;

												if (partOfGameMapPlayer[i + 1, setUpShipCoordinateY].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY + 1].PartIsFull == true ||
												   partOfGameMapPlayer[i + 1, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i + 1, setUpShipCoordinateY + 1].PartIsFull == true)
												{
													leftRightFrontPartIsEmpty = false;

													break;
												}
											}


											if (leftRightFrontPartIsEmpty)
											{
												for (int x = setUpShipCoordinateX; x < setUpShipCoordinateX + 2; x++)
												{
													if (partOfGameMapPlayer[x, setUpShipCoordinateY].PartIsFull == false)
													{
														partOfGameMapPlayer[x, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "D";

														partOfGameMapPlayer[x, setUpShipCoordinateY].PartIsFull = true;

														partOfGameMapPlayer[x, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = true;
													}

												}

												tryingToSetUpShip = true;
											}
											else
											{
												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
											}
										}
										else
										{
											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
										}

										Console.Clear();

										RenderPlayerBattleMap(11, 11, partOfGameMapPlayer);

										break;
									}

								case ConsoleKey.LeftArrow:
									{
										tryingToPushArrowKey = true;

										if (setUpShipCoordinateY - 2 > 0)
										{
											for (int i = setUpShipCoordinateY; i > setUpShipCoordinateY - 2; i--)
											{
												leftRightFrontPartIsEmpty = true;

												if (partOfGameMapPlayer[i - 1, setUpShipCoordinateY].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY + 1].PartIsFull == true ||
												   partOfGameMapPlayer[i - 1, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i + 1, setUpShipCoordinateY - 1].PartIsFull == true)
												{
													leftRightFrontPartIsEmpty = false;

													break;
												}
											}

											if (leftRightFrontPartIsEmpty)
											{
												for (int y = setUpShipCoordinateY; y > setUpShipCoordinateY - 2; y--)
												{
													partOfGameMapPlayer[setUpShipCoordinateX, y].PartWhoContainWhatUserAreWatcing = "D";

													partOfGameMapPlayer[setUpShipCoordinateX, y].PartIsFull = true;

													partOfGameMapPlayer[setUpShipCoordinateX, y].PartHullOfShipIsNotDestroed = true;
												}

												tryingToSetUpShip = true;
											}
											else
											{
												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
											}
										}
										else
										{
											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
										}

										Console.Clear();

										RenderPlayerBattleMap(11, 11, partOfGameMapPlayer);

										break;
									}

								case ConsoleKey.RightArrow:
									{
										tryingToPushArrowKey = true;

										if (setUpShipCoordinateY + 2 < 11)
										{
											for (int i = setUpShipCoordinateY; i < setUpShipCoordinateY + 2; i++)
											{
												leftRightFrontPartIsEmpty = true;

												if (partOfGameMapPlayer[i + 1, setUpShipCoordinateY].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY - 1].PartIsFull == true ||
												   partOfGameMapPlayer[i, setUpShipCoordinateY + 1].PartIsFull == true ||
												   partOfGameMapPlayer[i - 1, setUpShipCoordinateY + 1].PartIsFull == true ||
												   partOfGameMapPlayer[i + 1, setUpShipCoordinateY + 1].PartIsFull == true)
												{
													leftRightFrontPartIsEmpty = false;

													break;
												}
											}

											if (leftRightFrontPartIsEmpty)
											{
												for (int y = setUpShipCoordinateY; y < setUpShipCoordinateY + 2; y++)
												{
													partOfGameMapPlayer[setUpShipCoordinateX, y].PartWhoContainWhatUserAreWatcing = "D";

													partOfGameMapPlayer[setUpShipCoordinateX, y].PartIsFull = true;

													partOfGameMapPlayer[setUpShipCoordinateX, y].PartHullOfShipIsNotDestroed = true;
												}

												tryingToSetUpShip = true;
											}
											else
											{
												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

												partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
											}
										}
										else
										{
											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = false;

											partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;
										}

										Console.Clear();

										RenderPlayerBattleMap(11, 11, partOfGameMapPlayer);

										break;
									}
							}
						}

					}
				}

				if (countOfSetUpedShips == 6 || countOfSetUpedShips == 7 || countOfSetUpedShips == 8 || countOfSetUpedShips == 9)
				{
					bool tryingToSetUpShip = false;

					while (!tryingToSetUpShip)
					{
						setUpShipCoordinateX = 0;

						stringCoordinateY = "";

						setUpShipCoordinateY = 0;

						bool tryingToSetUpXCoordinate = false;

						while (!tryingToSetUpXCoordinate)
						{
							Console.WriteLine("Введите циферную координату");

							while (!int.TryParse(Console.ReadLine(), out setUpShipCoordinateX))
							{
								Console.WriteLine("Вводите только цифры");

								Console.WriteLine("Введите циферную координату");
							}

							foreach (KeyValuePair<int, int> keyValue in dictionaryForNumbers)
							{
								if (keyValue.Key == setUpShipCoordinateX)
								{
									setUpShipCoordinateX = keyValue.Value;

									break;
								}
							}

							if (setUpShipCoordinateX > 1 && setUpShipCoordinateX < 10)
							{
								tryingToSetUpXCoordinate = true;
							}
							else
							{
								Console.WriteLine("Еще раз");
							}
						}


						bool tryingToSetUpYCoordinate = false;

						while (!tryingToSetUpYCoordinate)
						{
							Console.WriteLine("Введите буквенную координату");

							tryingToReadYCoordinate = false;

							while (!tryingToReadYCoordinate)
							{
								stringCoordinateY = Console.ReadLine();

								foreach (KeyValuePair<string, int> keyValue in dictionaryForLetters)
								{
									if (keyValue.Key == stringCoordinateY)
									{
										setUpShipCoordinateY = keyValue.Value;

										tryingToReadYCoordinate = true;

										break;
									}
								}

								if (!tryingToReadYCoordinate)
								{
									Console.WriteLine("Вводите только указанные на поле буквы");

									Console.WriteLine("Введите буквенную координату");
								}
							}

							if (setUpShipCoordinateY > 1 && setUpShipCoordinateY < 10)
							{
								if (partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX + 1, setUpShipCoordinateY].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX - 1, setUpShipCoordinateY].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY + 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY - 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX - 1, setUpShipCoordinateY - 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX - 1, setUpShipCoordinateY + 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX + 1, setUpShipCoordinateY - 1].PartIsFull == false &&
									partOfGameMapPlayer[setUpShipCoordinateX - 1, setUpShipCoordinateY + 1].PartIsFull == false)
								{
									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "B";

									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = true;

									partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = true;

									tryingToSetUpYCoordinate = true;

									tryingToSetUpShip = true;
								}
								else
								{
									Console.WriteLine("Либо не правильное значение, либо попытка установить корабль поверх уже существующего");
								}
							}
							else
							{
								Console.WriteLine("Либо не правильное значение, либо попытка установить корабль поверх уже существующего");
							}
						}

						Console.Clear();

						RenderPlayerBattleMap(11, 11, partOfGameMapPlayer);

					}
				}
			}

			SetUpComputerShips(partOfGameMapPlayer, partOfGameMapComputer);

			Console.Clear();

			RenderBattleMaps(11, 11, partOfGameMapPlayer, partOfGameMapComputer);

			bool endGame = false;

			bool isPlayerTurn = true;

			bool tryingToFireing = true;

			bool botTryingToFire = false;

			bool allPlayerShipsAreDestroed = false;

			bool allBotShipsAreDestroed = false;

			Random rnd = new Random();

			while (!endGame)
			{
				if (isPlayerTurn)
				{					
					while (tryingToFireing)
					{
						Console.WriteLine("Введите координаты для открытия огня");

						setUpShipCoordinateX = 0;

						stringCoordinateY = "";

						setUpShipCoordinateY = 0;

						bool tryingToSetUpXCoordinate = false;

						while (!tryingToSetUpXCoordinate)
						{
							Console.WriteLine("Введите циферную координату");

							while (!int.TryParse(Console.ReadLine(), out setUpShipCoordinateX))
							{
								Console.WriteLine("Вводите только цифры");

								Console.WriteLine("Введите циферную координату");
							}

							foreach (KeyValuePair<int, int> keyValue in dictionaryForNumbers)
							{
								if (keyValue.Key == setUpShipCoordinateX)
								{
									setUpShipCoordinateX = keyValue.Value;

									break;
								}
							}

							if (setUpShipCoordinateX > 0 && setUpShipCoordinateX <= 10)
							{
								tryingToSetUpXCoordinate = true;
							}
							else
							{
								Console.WriteLine("Еще раз");
							}
						}


						bool tryingToSetUpYCoordinate = false;

						while (!tryingToSetUpYCoordinate)
						{
							Console.WriteLine("Введите буквенную координату");

							tryingToReadYCoordinate = false;

							while (!tryingToReadYCoordinate)
							{
								stringCoordinateY = Console.ReadLine();

								foreach (KeyValuePair<string, int> keyValue in dictionaryForLetters)
								{
									if (keyValue.Key == stringCoordinateY)
									{
										setUpShipCoordinateY = keyValue.Value;

										tryingToReadYCoordinate = true;

										break;
									}
								}

								if (!tryingToReadYCoordinate)
								{
									Console.WriteLine("Вводите только указанные на поле буквы");

									Console.WriteLine("Введите буквенную координату");
								}
							}

							if (setUpShipCoordinateY > 0 && setUpShipCoordinateY <= 10)
							{
								if(partOfGameMapComputer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull == true &&
								   partOfGameMapComputer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed == true)
								{
									partOfGameMapComputer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "0";

									partOfGameMapComputer[setUpShipCoordinateX, setUpShipCoordinateY].PartIsFull = true;

									partOfGameMapComputer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;

									tryingToSetUpYCoordinate = true;

									Console.Clear();

									RenderBattleMaps(11, 11, partOfGameMapPlayer, partOfGameMapComputer);
								}
								else
								{
									partOfGameMapComputer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "X";

									tryingToSetUpYCoordinate = true;

									tryingToFireing = false;

									isPlayerTurn = false;

									botTryingToFire = true;

									Console.Clear();

									RenderBattleMaps(11, 11, partOfGameMapPlayer, partOfGameMapComputer);
								}								
							}
							else
							{
								Console.WriteLine("Ложное целеуказание");
							}
						}
					}
				}
				
				if(botTryingToFire)
				{
					while(botTryingToFire)
					{
						setUpShipCoordinateX = GenerateDigit(rnd);

						setUpShipCoordinateY = GenerateDigit(rnd);

						if (partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed)
						{
							partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartHullOfShipIsNotDestroed = false;

							partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "O";

							Console.Clear();

							RenderBattleMaps(11, 11, partOfGameMapPlayer, partOfGameMapComputer);
						}
						else
						{
							partOfGameMapPlayer[setUpShipCoordinateX, setUpShipCoordinateY].PartWhoContainWhatUserAreWatcing = "X";

							botTryingToFire = false;

							tryingToFireing = true;

							isPlayerTurn = true;

							Console.Clear();

							RenderBattleMaps(11, 11, partOfGameMapPlayer, partOfGameMapComputer);
						}
					}
				}

				allPlayerShipsAreDestroed = true;

				allBotShipsAreDestroed = true;

				for (int i = 1; i < 11; i++)
				{
					for (int j = 1; j < 11; j++)
					{
						if(partOfGameMapPlayer[i,j].PartHullOfShipIsNotDestroed == true)
						{
							allPlayerShipsAreDestroed = false;
						}

						if (partOfGameMapComputer[i, j].PartHullOfShipIsNotDestroed == true)
						{
							allBotShipsAreDestroed = false;
						}
					}
				}

				if (allBotShipsAreDestroed)
				{
					Console.WriteLine("Противник уничтожен. Вы победили!");

					endGame = true;
				}

				if(allPlayerShipsAreDestroed)
				{
					Console.WriteLine("Ваш флот уничтожен. Вы проиграли сражение!");

					endGame = true;
				}
				
			}
		}
	}
}
